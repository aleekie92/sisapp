<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classes_id')->unsigned();
            $table->foreign('classes_id')->references('id')->on('classes')->onDelete('cascade');
            $table->integer('domtories_id')->unsigned();
            $table->foreign('domtories_id')->references('id')->on('domtories')->onDelete('cascade');
            $table->integer('class_roll_over_id')->unsigned();
            $table->foreign('class_roll_over_id')->references('id')->on('class_roll_over')->onDelete('cascade');
            $table->integer('exam_id')->unsigned();
            $table->foreign('exam_id')->references('id')->on('exams')->onDelete('cascade');
            $table->integer('subjects_id')->unsigned();
            $table->foreign('subjects_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->string('name');
            $table->string('parent_name');
            $table->string('parent_phone');
            $table->string('gender');
            $table->string('home_address');
            $table->string('dob');
            $table->string('county');
            $table->string('city');
            $table->string('photo');
            $table->string('email')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}