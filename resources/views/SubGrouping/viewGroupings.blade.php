@extends('app')
@section('content')
    <div id="action-buttons">
        <a href="{{url('subGroup/create')}}" class="btn btn-success">new Group</a>
    </div>
    <h1 align="center"><strong><u>all subject Groups</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="1px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Group Name</th>
            <th >Combination</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $group)
            <tr>
                <td>{{ $group->id }}</td>
                <td>{{ $group->groupName }}</td>
                <td>{!! ($group->description) !!}</td>
                <td colspan="3">
                <td><a href="{{url('Resources/'.$group->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['Resources.destroy', $group->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['Resources.show', $group->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop