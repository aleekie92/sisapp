@extends('app')
@section('content')
    <div class='col-lg-5 col-lg-offset-3'>

        <div class='form-group'>
    <h><i class="pageheading"></i><u>new Subject Group</u></h>
    {!! Form::open(['method'=>'POST', 'action'=>'SubjectsGroupsController@store']) !!}
    {{ csrf_field() }}
    <div>
        {!! Form::label('groupName','Subject Group Name:') !!}
        {!! Form::text('groupName',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group" style="align-content: center">
        {!! Form::label('description','Combination:') !!}
        {!! Form::text('description',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Save', ['class'=>'btn btn-primary form-control']) !!}
    </div>
    @include('errors.list')
    </div>
    {!! Form::close() !!}
    </div>
@stop