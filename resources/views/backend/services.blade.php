@extends('app')

@section('content')
    @if(Auth::user())
    <div id="wrapper">


        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center">
                        <img src="{{ asset('images/logo.png')}}" class="user-image img-responsive"/>
                    </li>
                    <li><a href="{{url('/students')}}" class="list-group-item">Students</a></li>
                    <li><a href="#" class="list-group-item">Prefects</a></li>
                    <li><a href="#" class="list-group-item">Staff Members</a></li>
                    <li><a href="#" class="list-group-item">Class Teachers</a></li>
                    <li><a href="#" class="list-group-item">School Workers</a></li>
                    <li><a href="#" class="list-group-item">HODS</a></li>
                    <li><a href="#" class="list-group-item">School Projects</a></li>
                    <li><a href="#" class="list-group-item">BOG Members</a></li>
                    <li><a href="{{url('/Resources')}}" class="list-group-item" id="linkid">Resources</a></li>
                    <li><a href="{{url('/exams')}}" class="list-group-item">Exams</a></li>
                    <li><a href="{{url('/classes')}}" class="list-group-item">Classes</a></li>
                    <li><a href="{{url('/rollOver')}}" class="list-group-item">Class Roll Over</a></li>
                    <li><a href="{{url('/domitories')}}" class="list-group-item">Domitories</a></li>
                    <li><a href="{{url('/subjects')}}" class="list-group-item">Subjects</a></li>
                    <li><a href="{{url('/subGroup')}}" class="list-group-item">Subject Groups</a></li>
                    <li class="icon">
                        <a href="javascript:void(0);" onclick="myFunction()">&#9776;</a>
                    </li>
                </ul>

            </div>
        </nav>
        @else
                @include('home')
                @endif

        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >

            <div id="page-inner">
                <div class="row">
                    <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-tachometer"></i>
                </span>
                            <div class="text-box" >
                                <p class="main-text">12</p>
                                <p class="text-muted">KNEC</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-users"></i>
                </span>
                            <div class="text-box" >
                                <p class="main-text">30</p>
                                <p class="text-muted">HISTORY</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-8 col-xs-8">
                        <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-bullseye"></i>
                </span>
                            <div class="text-box" >
                                <p class="main-text">5</p>
                                <p class="text-muted">COMING SOON</p>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /. ROW  -->
                <hr />
                <div class="row">

                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="panel back-dash">
                            <i class="fa fa-cogs fa-3x"></i><strong> &nbsp;Today's Activities</strong>
                            <p class="text-muted">
                                developers option.....developers option.....developers option.....
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-back noti-box">
                            <div class="text-box" >
                                <p class="main-text">About all school system</p>
                                <p class="text-muted">Version 1.0</p>
                                <hr />
                                <p class="text-muted">
                                <span class="text-muted color-bottom-txt"></i>
                                    sisapp....sisapp....sisapp....sisapp....sisapp....sisapp....
                               </span>
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="panel back-dash">
                            <i class="fa fa-bar-chart-o fa-3x"></i><strong> &nbsp; Sales Summaries</strong>
                            <p class="text-muted">
                                Summaries Sales Summaries Sales Summaries Sales Summaries Sales Summaries Sales Summaries</p>
                        </div>
                    </div>

                </div>
                <!-- /. ROW  -->

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
@endsection
