@extends('app')
@section('content')
    <div id="action-buttons">
        <a href="{{url('rollOver/create')}}" class="btn btn-success">update your class</a>
    </div>
    <h1 align="center"><strong><u>all Class Roll Over</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="1px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Form</th>
            <th >Stream Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $roll)
            <tr>
                <td>{{ $roll->id }}</td>
                <td>{{ $roll->form }}</td>
                <td>{!! ($roll->Stream) !!}</td>
                <td colspan="3">
                <td><a href="{{url('classes/'.$roll->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['subjects.destroy', $roll->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['subjects.show', $roll->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop