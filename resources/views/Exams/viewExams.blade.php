@extends('app')
@section('content')
    <div id="action-buttons">
        <a href="{{url('exams/create')}}" class="btn btn-success">new subject</a>
    </div>
    <h1 align="center"><strong><u>all Exams</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="1px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Exam Name:</th>
            <th >ClassId</th>
            <th >Year</th>
            <th >Term</th>
            <th >Upload</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $exam)
            <tr>
                <td>{{ $exam->id }}</td>
                <td>{{ $exam->name }}</td>
                <td>{!! ($exam->classes_id) !!}</td>
                <td>{{ $exam->year }}</td>
                <td>{{ $exam->term }}</td>
                <td>{{ $exam->url_upload }}</td>
                <td colspan="3">
                <td><a href="{{url('subjects/'.$exam->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['subjects.destroy', $exam->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['subjects.show', $exam->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop