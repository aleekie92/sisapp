@extends('app')
@section('content')

    <div class='col-lg-5 col-lg-offset-3'>

        @if($errors->has())
            @foreach ($errors->all() as $error)
                <div class='bg-danger alert'>{{ $error }}</div>
            @endforeach
        @endif
        <div class='form-group'>
            <h align="center"><i class="pageheading"></i><u><strong>new Exam</strong></u></h>
            {!! Form::open(['method'=>'POST', 'action'=>'ExamsController@store']) !!}
            {{ csrf_field() }}
            <div>
                {!! Form::label('name','Exam Name:') !!}
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>
            <div class="form-group" style="align-content: center">
                {!! Form::label('classes_id','subject Group Id:') !!}
                {!! Form::select('classes_id',$class,null,['class'=>'form-control']) !!}
            </div>
            <div>
                {!! Form::label('year','Year:') !!}
                {!! Form::text('year',null,['class'=>'form-control']) !!}
            </div>
            <div>
                {!! Form::label('term','Term:') !!}
                {!! Form::text('term',null,['class'=>'form-control']) !!}
            </div>
            <div>
                {!! Form::label('url_upload','Upload Exam:') !!}
                <input type="button" name="url_upload" label="browse"><br/>
            </div>
            <div class="form-group">
                {!! Form::submit('Save', ['class'=>'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop