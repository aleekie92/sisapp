@extends('app')
@section('content')
   <div id="action-buttons">
    <a href="{{url('Resources/create')}}" class="btn btn-success">new Resources</a>
   </div>
    <h1 align="center"><strong><u>all Resources</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="1px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Resources Name</th>
            <th >Resources details</th>
            <th>Resources status</th>
            <th>Availability status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $resource)
            <tr>
                <td>{{ $resource->id }}</td>
                <td>{{ $resource->name }}</td>
                <td>{!! ($resource->details) !!}</td>
                <td>{{ $resource->created_at->diffForHumans() }}</td>
                <td>
                    <div class="form-group">
                        {!! Form::checkbox('content_removed', $resource->content_removed,  $resource->content_removed) !!}
                        {!! Form::label('content_removed',  'ownership transfered/sold') !!}
                    </div>

                </td>
                <td colspan="3">
                <td><a href="{{url('Resources/'.$resource->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['Resources.destroy', $resource->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['Resources.show', $resource->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop