@extends('app')
@section('content')
    <div class='col-lg-5 col-lg-offset-3'>

        @if($errors->has())
            @foreach ($errors->all() as $error)
                <div class='bg-danger alert'>{{ $error }}</div>
            @endforeach
        @endif
        <div class='form-group'>

    <h><i class="pageheading"></i><u>new resources</u></h>
    {!! Form::open(['method'=>'POST', 'action'=>'ResourcesController@store']) !!}
    {{ csrf_field() }}
    <div>
        {!! Form::label('name','Resources Name:') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>

    <div>
        {!! Form::label('details','Resources Details:') !!}
        {!! Form::textarea('details',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Save', ['class'=>'btn btn-primary form-control']) !!}
    </div>
   </div>
    {!! Form::close() !!}
    </div>
@stop