<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm viewing</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('Resources') }}">Resources Alert</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('Resources/create') }}">Create a Resource</a>
        </ul>
    </nav>

    <div class="jumbotron text-center">
        <p>
            <strong>Name:{{ name }}</strong><br>
            <strong>details:{{ details }}</strong>
        </p>
    </div>

</div>
</body>
</html>