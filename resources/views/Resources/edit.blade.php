<html>
<head>
    <title>Editing Resources</title>
    <link rel="stylesheet" href={{ asset('/css/bootstrap.min.css') }}>

</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('Resources') }}">Resources Alert</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('Resources') }}">View All Resources</a></li>
            <li><a href="{{ URL::to('Resources/create') }}">Create a Resources</a>
        </ul>
    </nav>

    <h1>Edit</h1>
    <!-- if there are creation errors, they will show here -->
    {{ Html::ul($errors->all()) }}


    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('details', 'Details:') }}
        {{ Form::email('details', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
</html>