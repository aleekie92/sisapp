@extends('app')
@section('content')
    <html>
    <head>
        <title>loging in</title>
      <style>
          .container{
              text-align: center;
              display: table-cell;
              vertical-align: middle;
              alignment: center;
              margin: 0 auto;


          }
          .title{
              font-size: 30px;
          }
          .content {
              background-color: darkcyan; /* just for the demo */
              display: inline-block;
              text-align: center;
          }

          input{
              width: 40%;

              padding-bottom: 4px;
              border-radius: 3px;
              margin-bottom: 10px;
              border: thin solid blueviolet;
              font-size: 15px;
          }
          button{
              height: 40px;
              width: 20%;
              border-radius: 4px;
              background-color: rgba(73, 73, 73, 0.92);
              border-width: 1px;
          }
          legend{
              font-size: 20px;
              color: darkblue;
          }
      </style>
        </head>
<body background="images/home.png">
  <div class="container-fluid">
      <div class="content col-md-8 col-md-offset-2">
          <div class="title">All schools System</div>
          <fieldset style="border-width: thin; border-radius: 10px; ">
              <legend>login here</legend>
              <div class="panel-body">
                  @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <strong>Whoops!</strong> There were some problems with your inputs.<br><br>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

              <form class="form-horizontal" method="POST" action="{{url('/auth/login')}}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="">
                      <input type="email" placeholder="email@example.com" name="email"><br/>
                      <input type="password" placeholder="enter your password" name="password"><br/>

                      <div class="form-group">
                          <div class="col-md-4 col-md-offset-3">
                                  <input type="checkbox" name="remember">
                                  <label>Remember Me</label>
                          </div>

                      </div>
                      <button type="submit">LOGIN</button>
                      <br/><a  class="btn btn-link" href="{{ url('/password/email') }}" style="color: red">Forgot Your Password?</a>
                  </div>
                  </form>

                </div>
          </fieldset>
      </div>
  </div>
    </body>
@stop

