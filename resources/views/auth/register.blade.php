@extends('app')
@section('content')
 <title>Sign Up</title>
        <!-- Latest compiled and minified CSS -->
        <style>
            .container{
                text-align: center;
                display: table-cell;
                vertical-align: middle;
                alignment: center;
                margin: 0 auto;


            }
            .title{
                font-size: 30px;
                text-align: center;
            }
            .content {
                background-color: darkcyan; /* just for the demo */
                display: inline-block;
                width: 800px;
                text-align: left;

            }

            input{
                width: 80%;
                padding-bottom: 4px;
                border-radius: 3px;
                margin-bottom: 10px;
                border: thin solid blueviolet;
                font-size: 15px;
            }
            button{
                height: 40px;
                width: 20%;
                border-radius: 4px;
                background-color: green;
                border-width: 1px;
            }
            legend{
                font-size: 20px;
                color: darkblue;
            }
        </style>
    <div class="container-fluid">
        <div class="content col-md-8 col-md-offset-2">
            <div class="title">All schools System</div>
            <fieldset style="border-width: thin; border-radius: 10px; ">
                <legend style="text-align: center">Register Here</legend>
                <div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="post" action="{{ url('/auth/register') }}" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class=" "><label class="control-label">User name:</label>
                                <div >
                                <input type="text" class="form-control" name="name">
                            </div>

                           <label class="control-label">Email:</label>
                            <div >
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>

                            <label class="control-label">Password:</label>
                            <div >
                                <input type="password" class="form-control" name="password">
                            </div>

                            <label class="control-label">Confirm Password:</label>
                            <div>
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>

                            <button type="submit">Register</button>
                        </div>
                    </form>
                </div>
            </fieldset>

        </div>
    </div>
    @stop