@extends('app')
@section('content')
    <div class='col-lg-5 col-lg-offset-3'>

        @if($errors->has())
            @foreach ($errors->all() as $error)
                <div class='bg-danger alert'>{{ $error }}</div>
            @endforeach
        @endif
        <div class='form-group'>
    <h><i class="pageheading"></i><u>new student</u></h>
    {!! Form::open(['method'=>'POST', 'action'=>'StudentsController@store']) !!}
    {{ csrf_field() }}
    <div class="form-group" style="align-content: center">
        {!! Form::label('classes_id','Class Id:') !!}
        {!! Form::select('classes_id',['1','2'],null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group" style="align-content: center">
        {!! Form::label('domtories_id','Domitory Id:') !!}
        {!! Form::select('domtories_id',['1','2'],null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group" style="align-content: center">
        {!! Form::label('class_roll_over_id','Class Roll Over Id:') !!}
        {!! Form::select('class_roll_over_id',['1','2'],null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group" style="align-content: center">
        {!! Form::label('exam_id','Exam Id:') !!}
        {!! Form::select('exam_id',['1','2'],null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group" style="align-content: center">
        {!! Form::label('subjects_id','Subjects Id:') !!}
        {!! Form::select('subjects_id',['1','3'],null,['class'=>'form-control']) !!}
    </div>
    <div>
        {!! Form::label('name','Students Name:') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>

    <div>
        {!! Form::label('reg_no','Registration Number:') !!}
        {!! Form::text('reg_no',null,['class'=>'form-control']) !!}
    </div>

    <div>
        {!! Form::label('parent_name','Parents Name:') !!}
        {!! Form::text('parent_name',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group" style="align-content: center">
        {!! Form::label('parent_phone','Parents phone:') !!}
        {!! Form::text('parent_phone',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group" style="alignment: center">
        {!! Form::label('gender','Gender:') !!}
        {!! Form::text('gender',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group" style="alignment: center">
        {!! Form::label('home_address','Home Address:') !!}
        {!! Form::text('home_address',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group" style="alignment: center">
        {!! Form::label('dob','Date Of Birth:') !!}
        {!! Form::text('dob',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group" style="alignment: center">
        {!! Form::label('county','County:') !!}
        {!! Form::text('county',null,['class'=>'form-control']) !!}
    </div>

    <div class="form-group" style="alignment: center">
        {!! Form::label('city','City:') !!}
        {!! Form::text('city',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group" style="alignment: center">
        {!! form::label('photo','Photo:') !!}
        {!! form::text('photo',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group" style="alignment: center">
        {!! Form::label('email','Email:') !!}
        {!! Form::text('email',null,['class'=>'form-control']) !!}
    </div>

    <div style="alignment: center">
        {!! Form::submit('save',['class'=>'btn btn-primary form-control']) !!}
    </div>

    </div>
    {!! Form::close() !!}}
    </div>
@stop