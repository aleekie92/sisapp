@extends('app')
@section('content')
    <div id="action-buttons">
        <a href="{{url('students/create')}}" class="btn btn-success">new student</a>
    </div>
    <h1 align="center"><strong><u>all Students</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="2px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Class</th>
            <th >Domitory</th>
            <th>Class Roll Over Id</th>
            <th >Exam</th>
            <th >Subject</th>
            <th >Students name</th>
            <th >Reg No</th>
            <th >Parents Name</th>
            <th >Parents Phone</th>
            <th >Gender</th>
            <th >Home Address</th>
            <th >DOB</th>
            <th >County</th>
            <th >City</th>
            <th >Photo</th>
            <th >Parents email</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $student)
            <tr>
                <td>{{ $student->id }}</td>
                <td>{{ $student->classes_id }}</td>
                <td>{{ $student->domtories_id }}</td>
                <td>{{ $student->class_roll_over_id }}</td>
                <td>{{ $student->exam_id }}</td>
                <td>{{ $student->subjects_id }}</td>
                <td>{{ $student->name }}</td>
                <td>{{ $student->reg_no }}</td>
                <td>{{ $student->parent_name }}</td>
                <td>{{ $student->parent_phone }}</td>
                <td>{{ $student->gender }}</td>
                <td>{{ $student->home_address }}</td>
                <td>{{ $student->dob }}</td>
                <td>{{ $student->county }}</td>
                <td>{{ $student->city }}</td>
                <td>{!! link_to($student->photo) !!} </td>
                <td>{{ $student->email }}</td>
                <td colspan="3">
                <td><a href="{{url('Resources/'.$student->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['Resources.destroy', $student->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['Resources.show', $student->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop