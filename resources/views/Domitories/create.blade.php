@extends('app')
@section('content')
    <div class='col-lg-5 col-lg-offset-3'>

        @if($errors->has())
            @foreach ($errors->all() as $error)
                <div class='bg-danger alert'>{{ $error }}</div>
            @endforeach
        @endif
        <div class='form-group'>
            <h align="center"><i class="pageheading"></i><u><strong>Add new Class</strong></u></h>
            {!! Form::open(['method'=>'POST', 'action'=>'DomitoriesController@store']) !!}
            {{ csrf_field() }}
            <div>
                {!! Form::label('name','Class Name:') !!}
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::submit('Save', ['class'=>'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop