@extends('app')
@section('content')
    <div id="action-buttons">
        <a href="{{url('domitories/create')}}" class="btn btn-success"><strong>new domitory</strong></a>
    </div>
    <h1 align="center"><strong><u>all Domitories</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="1px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Domitory Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $domitories)
            <tr>
                <td>{{ $domitories->id }}</td>
                <td>{{ $domitories->name }}</td>
                <td colspan="3">
                <td><a href="{{url('domitories/'.$domitories->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['subjects.destroy', $domitories->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['subjects.show', $domitories->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop