@extends('app')
@section('content')
    <div id="wrapper">
        <div id="page-inner">

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Ask us a question">
                    <span class="input-group-btn">
                     <button class="btn btn-primary" type="button">Ask</button>
                    </span>
                    </div><!-- /input-group -->
                </div>
                <div class="col-md-2"></div>
            </div>


            <div class="row">

                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div style="height: 490px; background-color: #dddddd;" class="panel back-dash">
                        <h3 style="color: #000;">REQUEST A QUOTE</h3>
                        {!! Form::open(array('url'=>'#','files'=>true)) !!}

                        <div  class="form-group"><p style="color: black;">
                                {!!  Form::label('name', 'Name:') !!}
                                {!!  Form::text('name', null, ['class'=>'form-control']) !!}


                                {!!  Form::label('Email', 'email:') !!}
                                {!!  Form::text('email', null, ['class'=>'form-control']) !!}


                                {!!  Form::label('pick_up_date', 'Pick up Date:') !!}<br/>
                                {!! Form::selectMonth ('month',date('m')) !!}
                                {!! Form::selectRange('date',1,31,date('d')) !!}
                                {!! Form::selectRange('year',date('Y'),date('Y')+3) !!}
                                <br/>

                                {!!  Form::label('return_date', 'Return Date:') !!}<br/>
                                {!! Form::selectMonth('month',date('m')) !!}
                                {!! Form::selectRange('date',1,31,date('d')) !!}
                                {!! Form::selectRange('year',date('Y'),date('Y')+3) !!}

                                <br/><br/>
                                {!!  Form::label('robot', 'I am not a robot:') !!}<br/>
                            {!!  Form::checkbox('I am not a Robot', null, ['class'=>'form-control']) !!}


                            <p>



                            {!!  Form::submit('Request a Quote', ['class'=>'btn btn-primary form-control']) !!}
                        </div>
                        {!! Form::close() !!}

                                <!--<img src="{{ asset('images/alliance_logo.png')}}" align="center" style="height:95px; top:5px;bottom:5px; margin:auto;">-->
                    </div>
                </div>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div style="height: 450px;" class="panel panel-back noti-box">
                        <div class="text-box" >



                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                    <li data-target="#myCarousel" data-slide-to="2"></li>
                                    <li data-target="#myCarousel" data-slide-to="3"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div  class="item active">
                                        <img src="images/slider/car4.jpg" alt="mercedez" width="100%" height="400px">
                                        <div class="carousel-caption">
                                        </div>
                                    </div>

                                    <div class="item">
                                        <img src="images/slider/car3.jpg" alt="chevrolet" width="100%" height="400px">
                                        <div class="carousel-caption">
                                        </div>
                                    </div>

                                    <div class="item">
                                        <img src="images/slider/car7.jpg" alt="Volkswagen" width="100%" height="400px">
                                        <div class="carousel-caption">
                                        </div>
                                    </div>

                                    <div class="item">
                                        <img src="images/slider/car5.jpg" alt="Toyota" width="100%" height="400px">
                                        <div class="carousel-caption">
                                        </div>
                                    </div>
                                </div>

                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>



                            <!--  <img src="{{ asset('images/ngori2.jpg')}}" align="center" style="height:450px; width: 100%; position: absolute;">
                                <label style="position: absolute; margin-top: 50px; z-index: 100; margin-left: 150px; font-size: 25px;">UNLOCK THE WORLD – WITH ALLIANCE CAR HIRE</label>

                                <div class="text-box" >
                                    <button style="position: absolute; color: red; margin-top: 180px; margin-left: 150px; width: 150px; height: 60px; border-radius: 10px;">BOOK NOW</button>
                                </div>
-->
                        </div>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-back noti-box">
                        <p  class="text-muted"><h4 >Free, easy & fast car hire and airport transfers in kenya</h4>
                    </div>
                </div>


                <div class="col-md-6 col-sm-12 col-xs-12">

                    <div  class="panel panel-back noti-box">
                        <h3 style="color: #2EA7EB;">Book - Selfdrive - Enjoy</h3>
                    </div>

                </div>

            </div>




            <div class="row">
                <div class="col-md-4 col-sm-8 col-xs-8">
                    <div class="panel panel-back noti-box">
                        <h3>SISAPP</h3>
                        <img src="{{ asset('images/img/car1.jpg')}}">
                        <p>
                        <p>Find cheap car hire fast and hit the road happy with Alliancecar.
                            Instantly best prices. Then book direct with us. It’s so easy.


                    </div>
                </div>
                <div class="col-md-4 col-sm-8 col-xs-8">
                    <div class="panel panel-back noti-box">
                        <h3>SCHOOLS</h3>
                        <img src="{{ asset('images/img/car2.jpg')}}">
                        <p>
                        <p>Don’t wait until you land to find your airport transfer.
                            Enter your details and we’ll help you choose a reliable car and get a great deal.
                            Then relax knowing it’s all taken care of.

                    </div>
                </div>
                <div class="col-md-4 col-sm-8 col-xs-8">
                    <div style="background-color: #98EFFC;" class="panel panel-back noti-box">

                        <div class="text-box" >
                            <p class="text-muted">WHY ALLIANCE CAR?</p>
                            <hr/>
                            <ul type="bullets">
                                <li>Simple and instant rental rate comparisons</li>
                                <li>Instant booking confirmation</li>
                                <li>No credit card details required</li>
                                <li>Lowest price guarantee</li>
                                <li>100% Kenyan based, owned and operated</li>
                                <li>Smart phone friendly </li>
                                <li>Outstanding Customer Service Team waiting to assist you</li>
                                <li>Free to Book and Cancel</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /. ROW  -->
            <hr />



            <div class="container">
                <div class="row">
                    <div ng-view class="col-xs-3"></div>
                    <div STYLE=" height: 150px;" ng-view class="col-xs-9 content">
                        <div style="height: 150px;" ng-view class="col-xs-3">
                            <a href="{{url('/frontend/bookings')}}"><img src={{asset('images/booking.jpg')}} alt="Logo"></a>





                        </div>
                        <div ng-view style="height: 150px;" class="col-xs-3">
                            <a href="#"><img src={{asset('images/quote.jpg')}} alt="Logo"></a>
                        </div>
                        <div style="height: 150px;" ng-view class="col-xs-3">
                            <a href="#"><img src={{asset('images/offer.jpg')}} alt="Logo"></a>
                        </div>
                    </div>
                    <div ng-view class="col-xs-3"></div>
                </div>
            </div>






            <div style="background-color: white;" class="row">

                <div style="background-color: white"  class="border row">
                    <div class="col-xs-12 col-sm-4 panel" style="background-color: white; height: 100px;">
                        <label>ALLIANCE CAR LIMITED</label><br/>
                        105784 NAIROBI<br/>
                        ANIVERSARY TOWERS<br/>
                        alliancecar@gmail.com

                    </div><!--end of column 1 -->
                    <div class="col-xs-6 col-sm-4 panel" style="background-color: white; height: 100px;">

                        <a href="#">PRIVACY POLICY</a><br/>
                        <a href="#">TERMS & CONDITIONS</a><br/>
                        <a href="#">PARTNER SITES</a><br/>

                    </div>

                    <div class="col-xs-6 col-sm-4 panel" style="background-color: white; height: 100px;">
                        <a href="#">CONTACT US</a><br/>
                        <a href="#">ABOUT US</a><br/>
                        <a href="#">FAQs</a><br/>
                        <a href="#">SITE MAP</a>
                    </div>
                </div>

            </div>
            <!--ROW -->
            <hr/>
            <div class="row">

                <div class="col-md-6 col-sm-12 col-xs-12">

                    <div class="panel panel-back noti-box">
                        <label>subscribe to our newsletter</label><p>
                        <div class="input-group custom-search-form">
                            {!! Form::open(['method'=>'GET','url'=>'#','class'=>'navbar-form navbar-left','role'=>'search'])  !!}


                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <br/>
                            <a href="{{ url('#') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Add</a>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-sm-12 col-xs-12">

                    <div  class="panel panel-back noti-box">
                        <a href="#"><img src="{{ asset('images/socialmedia/email.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/facebook.png')}}" align="center" style="height:55px;  padding: 5px;  top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/twitter.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/google_plus.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/pintrest.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        &nbsp &nbsp &nbsp
                        &nbsp &nbsp &nbsp
                        &nbsp &nbsp &nbsp &nbsp
                        <button style="" class="btn" type="button"><h5>Back to top</h5></button>
                    </div>

                </div>

            </div>
            <!-- /. ROW  -->

            <!-- /. PAGE INNER  -->
        </div>
    </div>
    @stop