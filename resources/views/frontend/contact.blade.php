@extends('app')
@section('content')
    <div id="wrapper">
        <div id="page-inner">


            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Ask us a question">
                    <span class="input-group-btn">
                     <button class="btn btn-primary" type="button">Ask</button>
                    </span>
                    </div><!-- /input-group -->
                </div>
                <div class="col-md-2"></div>
            </div>


            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <h3 style="color: #2f96b4;">Need more help?</h3>
                        <p>
                            To contact ALLIANCECAR directly via email please use one of the options below.
                        <p>
                            <label style="font-weight: bold;" >Reservations</label>
                            <br/>
                            reserve@alliancecar.com
                        <p>
                            <label style="font-weight: bold;" >Feedback</label>
                            <br>
                            feedback@alliancecar.com
                    </div>
                    <div class="col-md-4">
                        <h3 style="color: #2f96b4;">Phone bookings and enquiries</h3>
                        <br/>
                        <label>SAFARICOM:</label>
                        <P>
                            <label>AIRTEL:</label>
                        <P>
                            <label>YU:</label>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <hr/>
            <!-- /. ROW  -->

            <div style="background-color: white;" class="row">

                <div style="background-color: white"  class="border row">
                    <div class="col-xs-12 col-sm-4 panel" style="background-color: white; height: 100px;">
                        <label>ALLIANCE CAR LIMITED</label><br/>
                        105784 NAIROBI<br/>
                        ANIVERSARY TOWERS<br/>
                        alliancecar@gmail.com

                    </div><!--end of column 1 -->
                    <div class="col-xs-6 col-sm-4 panel" style="background-color: white; height: 100px;">

                        <a href="#">PRIVACY POLICY</a><br/>
                        <a href="#">TERMS & CONDITIONS</a><br/>
                        <a href="#">PARTNER SITES</a><br/>

                    </div>
                    <div class="col-xs-6 col-sm-4 panel" style="background-color: white; height: 100px;">
                        <a href="#">CONTACT US</a><br/>
                        <a href="#">ABOUT US</a><br/>
                        <a href="#">FAQs</a><br/>
                        <a href="#">SITE MAP</a>
                    </div>
                </div>

            </div>
            <!--ROW -->

            <div class="row">

                <div style="background-color: white" class="col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-back noti-box">
                        Footer 1
                    </div>
                </div>


                <div style="background-color: white" class="col-md-6 col-sm-12 col-xs-12">

                    <div  class="panel panel-back noti-box">
                        <a href="#"><img src="{{ asset('images/socialmedia/email.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/facebook.png')}}" align="center" style="height:55px;  padding: 5px;  top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/twitter.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/google_plus.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        <a href="#"><img src="{{ asset('images/socialmedia/pintrest.png')}}" align="center" style="height:55px; padding: 5px; top:5px;bottom:5px; margin:auto;"></a>
                        &nbsp &nbsp &nbsp
                        &nbsp &nbsp &nbsp
                        &nbsp &nbsp &nbsp &nbsp
                        <button style="" class="btn" type="button"><h5>Back to top</h5></button>
                    </div>

                </div>

            </div>
    </div>
@stop