<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>all schools system</title>

    <script src="{{asset("/js/bootstrap.min.js")}}" type="text/javascript"></script>

    <!-- Fonts -->
    <!--   <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'/> -->
    <!-- BOOTSTRAP STYLES-->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="{{ asset('/css/font-awesome/font-awesome.css') }}" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" />
    <link href="{{ asset('/css/navbar.css') }}" rel="stylesheet"/>
    <!-- <link href="{{ asset('/css/app.css') }}" rel="stylesheet"/> -->
    <link href="{{ asset('/css/datetimepicker/datetimepicker.min.css') }}" rel="stylesheet"/>

</head>
<body style="height: 100%;">

<nav class="navbar-default" role="navigation">

    <div class="collapse navbar-collapse" id="header">
        <ul class="nav navbar-nav" style="background: blue">
            <li class="pull-left"><img src="{{ asset('images/logo.png')}}" style="height:35px; position:relative;top:5px;bottom:5px; margin:auto;"></li>
            <li class="pull-left" style="color: white"><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i> Home</a></li>
            <li class="pull-left" style="color: white"><a href="{{ url('about') }}"><i class="glyphicon glyphicon-check"></i> About sisapp</a></li>
            <li class="pull-left" style="color: white"><a href="{{ url('help') }}"><i class="glyphicon glyphicon-filter"></i> FAQ</a></li>
            <li class="pull-left" style="color: white"><a href="{{ url('contact') }}"><i class="glyphicon glyphicon-phone"></i> Contact us</a></li>
            <li class="pull-left" style="color: white"><a href="{{ url('products') }}"><i class="glyphicon glyphicon-globe"></i> Our Services</a></li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            @if (Auth::guest())
                <li><a href="{{ url('/auth/login') }}"><i class="glyphicon glyphicon-log-in"></i> Login</a></li>
                <li><a href="{{ url('/auth/register') }}"><i class="glyphicon glyphicon-registration-mark"></i> Register</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->user_id }} <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('profile') }}">My Profile</a></li>
                        <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                    </ul>
                </li>

            @endif
        </ul>
    </div>
</nav>

<div class="row" style="margin-bottom: 50px; margin-top: 0px; left: 0px;width: 100%; padding-right: 10px;padding-left:20px;">
    @yield('content')
</div>


<div class="footer navbar-fixed-bottom">
    <div class="panel-footer" style="background-color: #032471">
        <div class="pull-left" style="position:absolute;"><img src="{{ asset('images/logo.png')}}" style="height:30px; position:absolute;top:20px;bottom:5px; margin:auto;"></div>
        <div class="pull-left" style="margin-left: 10%; color: white">All schools. Â© Copyright 2016</div>
        <div class="pull-right" style="margin-right: 10%;color: white">Login Name: Alex K</div>
        <div align="center"><a style="color:#FFFFFF;">Terms and Conditions</a></div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/jquery/2.1.3/jquery.min.js') }}"></script>
<script src="{{ asset('js/twitter-bootstrap/3.3.6/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/jquery/metisMenu.js') }}"></script>
<script src="{{ asset('/js/datetimepicker/datetimepicker.min.js') }}"></script>
<script src="{{ asset('/js/datetimepicker/datetimepicker.pt-BR.js') }}"></script>
<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="{{ asset('js/html5shiv/3.7.2/html5shiv.min.js') }}"></script>
<script src="{{ asset('js/respond/1.4.2/respond.min.js') }}"></script>
<![endif]-->
<script>
    $('flash-overlay-modal').modal();
</script>
<script>
    $('div.alert').not('.alert-important').delay(3000).slideUp(300);
</script>
</body>
</html>