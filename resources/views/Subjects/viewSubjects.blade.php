@extends('app')
@section('content')
    <div id="action-buttons">
        <a href="{{url('subjects/create')}}" class="btn btn-success">new subject</a>
    </div>
    <h1 align="center"><strong><u>all Resources</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="1px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Subject Name:</th>
            <th >Subjects Group Id</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $subjects)
            <tr>
                <td>{{ $subjects->id }}</td>
                <td>{{ $subjects->name }}</td>
                <td>{!! ($subjects->subjects_grouping_id) !!}</td>
                <td colspan="3">
                <td><a href="{{url('subjects/'.$subjects->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['subjects.destroy', $subjects->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['subjects.show', $subjects->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop