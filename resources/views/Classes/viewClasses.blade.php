@extends('app')
@section('content')
    <div id="action-buttons">
        <a href="{{url('classes/create')}}" class="btn btn-success">new class</a>
    </div>
    <h1 align="center"><strong><u>all Classes</u></strong></h1>

    @if(Session::has('message'));

    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <table class="table table-stripped table-bordered" border="1px" align="center">
        <thead>
        <tr class="bg-info" style="color: #032471">
            <th >id</th>
            <th >Class Name</th>
            <th >Stream Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $classes)
            <tr>
                <td>{{ $classes->id }}</td>
                <td>{{ $classes->name }}</td>
                <td>{!! ($classes->stream) !!}</td>
                <td colspan="3">
                <td><a href="{{url('classes/'.$classes->id.'/edit')}}" class="btn btn-warning">update</a></td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'DELETE', 'route'=>['subjects.destroy', $classes->id]]) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {!! Form::open(['method' => 'SHOW', 'route'=>['subjects.show', $classes->id]]) !!}
                        {!! Form::submit('show', ['class' => 'btn btn-show']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop