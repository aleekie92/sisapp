<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    protected $table="students";
    protected $fillable=[
        'classes_id',
        'domtories_id',
        'class_roll_over_id',
        'exam_id',
        'subjects_id',
        'name',
        'reg_no',
        'parent_name',
        'parent_phone',
        'gender',
        'home_address',
        'dob',
        'county',
        'city',
        'photo',
        'email'
    ];
}
