<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRollOver extends Model
{
    protected $table="Class_Roll_Over";
    protected $fillable=[
        'form',
        'stream'
    ];
}
