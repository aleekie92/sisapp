<?php

namespace App\Http\Controllers;

use App\SubjectsGrouping;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Subjects;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;

class SubjectsController extends Controller
{
    public function index(){
        $data = Subjects::all();
        return view('Subjects.viewSubjects', compact('data'));
    }
    public function create(){
        $subGroup= SubjectsGrouping::lists('groupName','id');
        return view('Subjects.create', compact('subGroup'));
    }
    public function show($id){
        $data = Subjects::find($id);

        // show the view and pass the data to it
        return View::make('Subjects.show')
            ->with('Subjects', $data);
    }
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'subjects_grouping_id' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('subjects')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new Subjects;
            $data->name = Input::get('name');
            $data->subjects_grouping_id = Input::get('subjects_grouping_id');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new subject!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = Subjects::find($id);

        // show the edit form and pass the nerd
        return View::make('subjects.edit')
            ->with('Subjects', $data);
    }

    public function update($id){
        $rules = array(
            'name'       => 'required',
            'subjects_grouping_id'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('subjects/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = Subjects::find($id);
            $data->name       = Input::get('name');
            $data->subjects_grouping_id      = Input::get('subjects_grouping_id');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully updated subjects!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('subjects');
        }
    }
    public function destroy($id){
        // delete
        $data = Subjects::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the subjects!');
        return Redirect::to('subjects');
    }
}
