<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Students;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;

class StudentsController extends Controller
{
    public function index(){
        $data = Students::all();
        return view('Students.viewStudents', compact('data'));
    }
    public function create(){
        //$resource= Resources::lists('name','details');
        return view('Students.create');
    }
    public function show($id){
        $data = Students::find($id);

        // show the view and pass the data to it
        return View::make('Students.show')
            ->with('Students', $data);
    }

    public function store(Request $request)
    {
        $rules = array(

            'name' => 'required',
            'parent_name' => 'required',
            'parent_phone' => 'required',
            'gender' => 'required',
            'home_address' => 'required',

        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('Students/create')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new Students;
            $data->classes_id = Input::get('classes_id');
            $data->domtories_id = Input::get('domtories_id');
            $data->class_roll_over_id = Input::get('class_roll_over_id');
            $data->exam_id = Input::get('exam_id');
            $data->subjects_id = Input::get('subjects_id');

            $data->name = Input::get('name');
            $data->parent_name = Input::get('parent_name');
            $data->parent_phone = Input::get('parent_phone');
            $data->gender = Input::get('gender');
            $data->home_address = Input::get('home_address');
            $data->dob = Input::get('dob');
            $data->county = Input::get('county');
            $data->city = Input::get('city');
            $data->photo = Input::get('photo');
            $data->email = Input::get('email');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new student!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = Students::find($id);

        // show the edit form and pass the nerd
        return View::make('Students.edit')
            ->with('Students', $data);
    }
    public function update($id){
        $rules = array(
            'class_id' => 'required',
            'domtories_id' => 'required',
            'class_roll_over_id' => 'required',
            'exam_id' => 'required',
            'subjects_id' => 'required',

            'name' => 'required',
            'parent_name' => 'required',
            'parent_phone' => 'required',
            'gender' => 'required',
            'home_address' => 'required',
            'dob' => 'required',
            'county' => 'required',
            'city' => 'required',
            'photo' =>'required',
           'email' => 'required||unique',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('Resources/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = Students::find($id);
            $data = new Students;
            $data->class_id = Input::get('class_id');
            $data->domtories_id = Input::get('domtories_id');
            $data->class_roll_over_id = Input::get('class_roll_over_id');
            $data->exam_id = Input::get('exam_id');
            $data->subjects_id = Input::get('subjects_id');

            $data->name = Input::get('name');
            $data->parent_name = Input::get('parent_name');
            $data->parent_phone = Input::get('parent_phone');
            $data->gender = Input::get('gender');
            $data->home_address = Input::get('home_address');
            $data->dob = Input::get('dob');
            $data->county = Input::get('county');
            $data->city = Input::get('city');
            $data->photo = Input::get('photo');
            $data->email = Input::get('email');
            $data->save();
            // redirect
            Session::flash('message', 'Successfully updated student!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('Students');
        }
    }
    public function destroy($id){
        // delete
        $data = Students::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the student!');
        return Redirect::to('Students');
    }
}
