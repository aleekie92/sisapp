<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Classes;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;


class ClassesController extends Controller
{
    public function index(){
        $data = Classes::all();
        return view('Classes.viewClasses', compact('data'));
    }
    public function create(){
        //$subGroup= SubjectsGrouping::lists('groupName','id');
        return view('Classes.create');
    }
    public function show($id){
        $data = Classes::find($id);

        // show the view and pass the data to it
        return View::make('Classes.show')
            ->with('classes', $data);
    }
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'stream' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('classes')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new Classes();
            $data->name = Input::get('name');
            $data->stream = Input::get('stream');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new class!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = Classes::find($id);

        // show the edit form and pass the nerd
        return View::make('Classes.edit')
            ->with('classes', $data);
    }

    public function update($id){
        $rules = array(
            'name'       => 'required',
            'stream'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('classes/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = Subjects::find($id);
            $data->name       = Input::get('name');
            $data->stream      = Input::get('stream');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully updated subjects!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('classes');
        }
    }
    public function destroy($id){
        // delete
        $data = Classes::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the classes!');
        return Redirect::to('classes');
    }
}
