<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domitories;
use App\Http\Requests;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;

class DomitoriesController extends Controller
{
    public function index(){
        $data = Domitories::all();
        return view('Domitories.viewDomitories', compact('data'));
    }
    public function create(){
        //$subGroup= SubjectsGrouping::lists('groupName','id');
        return view('Domitories.create');
    }
    public function show($id){
        $data = Domitories::find($id);

        // show the view and pass the data to it
        return View::make('Domitories.show')
            ->with('domitories', $data);
    }
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('domitories')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new Domitories();
            $data->name = Input::get('name');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new domitory!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = Domitories::find($id);

        // show the edit form and pass the nerd
        return View::make('Domitories.edit')
            ->with('domitories', $data);
    }

    public function update($id){
        $rules = array(
            'name'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('domitories/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = Subjects::find($id);
            $data->name       = Input::get('name');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully updated domitory!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('domitories');
        }
    }
    public function destroy($id){
        // delete
        $data = Domitories::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the domitory!');
        return Redirect::to('classes');
    }
}
