<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Resources;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;

class ResourcesController extends Controller
{
    public function index(){
        $data = Resources::all();
        return view('Resources.view', compact('data'));
    }
   public function create(){
      //$resource= Resources::lists('name','details');
      return view('Resources.create');
   }
    public function show($id){
        $data = Resources::find($id);

        // show the view and pass the data to it
        return View::make('Resources.show')
            ->with('Resources', $data);
    }
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'details' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('Resources/create')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new Resources;
            $data->name = Input::get('name');
            $data->details = Input::get('details');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new school resource!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = Resources::find($id);

        // show the edit form and pass the nerd
        return View::make('Resources.edit')
            ->with('Resources', $data);
    }

    public function update($id){
        $rules = array(
            'name'       => 'required',
            'details'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('Resources/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = Resources::find($id);
            $data->name       = Input::get('name');
            $data->details      = Input::get('details');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully updated resources!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('Resources');
        }
    }
    public function destroy($id){
        // delete
        $data = Resources::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the resource!');
        return Redirect::to('Resources');
    }
}
