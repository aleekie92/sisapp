<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Classes;
use App\Exams;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;


class ExamsController extends Controller
{ public function index(){
    $data = Exams::all();
    return view('Exams.viewExams', compact('data'));
}
    public function create(){
        $class= Classes::lists('name','id');
        return view('Exams.create', compact('class'));
    }
    public function show($id){
        $data = Exams::find($id);

        // show the view and pass the data to it
        return View::make('Exams.show')
            ->with('exams', $data);
    }
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'classes_id' => 'required',
            'year' => 'required',
            'term' => 'required',
            'url::upload' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('exams')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new Exams;
            $data->name = Input::get('name');
            $data->classes_id = Input::get('classes_id');
            $data->year = Input::get('year');
            $data->term = Input::get('term');
            $data->url_upload = Input::get('url_upload');

            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new exam!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = Exams::find($id);

        // show the edit form and pass the nerd
        return View::make('Exams.edit')
            ->with('exams', $data);
    }

    public function update($id){
        $rules = array(
            'name'       => 'required',
            'classes_id'      => 'required',
            'year'       => 'required',
            'term'       => 'required',
            'url_upload'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('exams/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = Exams::find($id);
            $data->name = Input::get('name');
            $data->classes_id = Input::get('classes_id');
            $data->year = Input::get('year');
            $data->term = Input::get('term');
            $data->url_upload = Input::get('url_upload');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully updated exam!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('exams');
        }
    }
    public function destroy($id){
        // delete
        $data = Exams::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the exam!');
        return Redirect::to('exams');
    } //
}
