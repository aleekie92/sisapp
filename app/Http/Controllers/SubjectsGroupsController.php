<?php

namespace App\Http\Controllers;

use App\SubjectsGrouping;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;

class SubjectsGroupsController extends Controller
{
    public function index(){
        $data = SubjectsGrouping::all();
        return view('SubGrouping.viewGroupings', compact('data'));
    }
    public function create(){
        //$resource= Resources::lists('name','details');
        return view('SubGrouping.create');
    }
    public function show($id){
        $data = SubjectsGrouping::find($id);

        // show the view and pass the data to it
        return View::make('SubGrouping.show')
            ->with('subGroup', $data);
    }
    public function store(Request $request)
    {
        $rules = array(
            'groupName' => 'required',
            'description' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('SubGroup')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new SubjectsGrouping;
            $data->groupName = Input::get('groupName');
            $data->description = Input::get('description');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new subject group!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = SubjectsGrouping::find($id);

        // show the edit form and pass the nerd
        return View::make('SubGrouping.edit')
            ->with('SubGroup', $data);
    }

    public function update($id){
        $rules = array(
            'groupName'       => 'required',
            'description'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('subGroup/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = Resources::find($id);
            $data->groupName       = Input::get('groupName');
            $data->description      = Input::get('description');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully updated resources!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('subGroup');
        }
    }
    public function destroy($id){
        // delete
        $data = SubjectsGrouping::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the subjects combination!');
        return Redirect::to('subGroup');
    }
}
