<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ClassRollOver;
use Validator;
use Input;
use Session;
use App\URL;
use View;
use Illuminate\Support\Facades\Redirect;


class ClassRollOverController extends Controller
{
    public function index(){
        $data = ClassRollOver::all();
        return view('ClassRollOver.viewRollClasses', compact('data'));
    }
    public function create(){
        //$subGroup= SubjectsGrouping::lists('groupName','id');
        return view('ClassRollOver.create');
    }
    public function show($id){
        $data = ClassRollOver::find($id);

        // show the view and pass the data to it
        return View::make('ClassRollOver.show')
            ->with('rollOver', $data);
    }
    public function store(Request $request)
    {
        $rules = array(
            'form' => 'required',
            'Stream' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('rollOver')
                ->withErrors($rules)
                ->withInput(Input::except('password'));
        } else {
            // store
            $data = new ClassRollOver();
            $data->form = Input::get('form');
            $data->Stream = Input::get('Stream');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully created a new class!');
            return
                $this->index();
        }
    }
    public function edit($id){
        $data = ClassRollOver::find($id);

        // show the edit form and pass the nerd
        return View::make('ClassRollOver.edit')
            ->with('rollOver', $data);
    }

    public function update($id){
        $rules = array(
            'form'       => 'required',
            'Stream'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('rollOver/'.$id.'/edit')
                ->withErrors($validator);
        } else {
            // store
            $data = ClassRollOver::find($id);
            $data->form       = Input::get('form');
            $data->Stream      = Input::get('Stream');
            $data->save();

            // redirect
            Session::flash('message', 'Successfully updated subjects!');
            /*return View::make('Resources.edit')
                ->with('Resources', $data);*/
            return Redirect::to('rollOver');
        }
    }
    public function destroy($id){
        // delete
        $data = ClassRollOver::find($id);
        $data->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the classes!');
        return Redirect::to('rollOver');
    }
}
