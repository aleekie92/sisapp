<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Illuminate\Support\Facades;

class PagesController extends Controller
{

function doLogout(){
    Auth::logout();
        return redirect('auth/login');
}
    public function home(){
        if(Auth::user()){
            //return \Auth::user()->name;
            return redirect('services');
        }
        else{
            return view('frontend.home');
        }

    }
   public function contact()
   {
       if (Auth::user()) {
           return redirect('services');
       } else {
           return view('frontend.contact');
       }
   }
    public function about()
    {
        if (Auth::user()) {
            return redirect('services');
        } else {
            return view('frontend.about');
        }
    }
    public function help()
    {
        if (Auth::user()) {
            return redirect('services');
        } else {
            return view('frontend.help');
        }
    }
    public function products()
    {
        if (Auth::user()) {
            return redirect('services');
        } else {
            return view('frontend.products');
        }
    }
    public function welcome(){
        return view('welcome');
    }
    public function profile(){
        return view('backend.profile');
    }
}
