<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::controllers([ 'Auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController']);
Route::get('/','PagesController@home');


Route::get('about','PagesController@about');
Route::get('help','PagesController@help');
Route::get('contact','PagesController@contact');
Route::get('products','PagesController@products');
Route::get('welcome','PagesController@welcome');
Route::get('profile','PagesController@profile');
//Route::get('/','PagesController@services');

Route::get('services', function () {
    return view('backend.services');
});

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');


Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::get('auth/register','UsersController@create');
Route::get('user/profile', [
    'as' => 'profile', 'uses' => 'UsersController@showProfile'
]);

Route::get('auth/logout', array('uses'=>'PagesController@doLogout'));

Route::resource('Resources', 'ResourcesController');
Route::resource('students','StudentsController');
Route::resource('subjects','SubjectsController');
Route::resource('subGroup','SubjectsGroupsController');
Route::resource('classes','ClassesController');
Route::resource('domitories','DomitoriesController');
Route::resource('rollOver','ClassRollOverController');
Route::resource('exams','ExamsController');








