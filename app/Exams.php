<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exams extends Model
{
    protected $table="exams";
    protected $fillable=[
        'name',
        'classes_id',
        'year',
        'term',
        'url_upload'
    ];
}
