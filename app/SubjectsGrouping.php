<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectsGrouping extends Model
{
    protected $table="subjects_grouping";
    protected $fillable=[
        'groupName',
        'description'
    ];
}
